Java FX - WS - Example
======================

　このプロジェクトは、Java FX で作成したクライアントサイドプログラムから Web Service（=WS）をリモートプロシージャコール風に呼び出すサンプルです。


実行方法
--------

　client および server それぞれのプロジェクト直下で以下のコマンドを実行してください。

    gradle bootRun


