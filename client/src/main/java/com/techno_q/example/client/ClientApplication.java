package com.techno_q.example.client;

import com.techno_q.example.client.controller.MainMenuController;
import com.techno_q.example.client.helper.I18nHelper;
import com.techno_q.example.service.Constants;
import com.techno_q.example.service.GreetingService;
import com.techno_q.example.service.UsersService;
import com.techno_q.example.service.AccountService;
import javafx.application.Application;
import javafx.stage.Stage;
import lombok.val;
import org.apache.cxf.configuration.security.ProxyAuthorizationPolicy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import java.util.Optional;

/**
 * クライアント・アプリケーションのエントリポイントです。
 */
@SpringBootApplication
@ImportResource({"classpath:META-INF/cxf/cxf.xml"})
public class ClientApplication extends Application {

    /**
     * Spring の Application Context です。
     */
    private static ConfigurableApplicationContext context;

    /**
     * アプリケーションのエントリポイントとなるメインメソッドです。
     *
     * @param args コマンドライン引数
     */
    public static void main(String[] args) {
        // jul によるロギングを SLF4J に載せるための設定
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        context = SpringApplication.run(ClientApplication.class, args);
        launch(args);
    }

    @Autowired
    private Settings settings;

    @Autowired
    private I18nHelper i18nHelper;

    @Override
    public void start(Stage primaryStage) throws Exception {
        val loader = context.getBean(SpringFXMLLoader.class);

        Optional<MainMenuController> cOpt = loader.loadFromResource("mainmenu.fxml", primaryStage);
        primaryStage.setResizable(false);
        primaryStage.setTitle(context.getBean(I18nHelper.class).getMessage("main.title"));
        cOpt.ifPresent(MainMenuController::showLogin);
    }

    @Override
    public void stop() throws Exception {
        context.close();
    }

    @Bean
    public AccountService provideAccountWS() {
        return getService(AccountService.class);
    }

    @Bean
    public GreetingService provideGreetingService() {
        return getService(GreetingService.class);
    }

    @Bean
    public UsersService provideUsersService() {
        return getService(UsersService.class);
    }

    private <T> T getService(Class<T> clazz) {
        val servletPath = Constants.CXF_SERVLET_PATH;
        @SuppressWarnings("unchecked")
        val result = (T) Optional.ofNullable(Constants.SERVICE_PATH_MAP.get(clazz)).map((path) -> {
            JaxWsProxyFactoryBean b = new JaxWsProxyFactoryBean();
            b.setServiceClass(clazz);
            b.setAddress(settings.getWebServicePrefix() + servletPath + path);
            b.getInInterceptors().add(new LoggingInInterceptor());
            b.getOutInterceptors().add(new LoggingOutInterceptor());

            return b.create();
        }).orElseThrow(() -> new IllegalStateException(i18nHelper.getMessage("error.service.path")));

        Optional.ofNullable(settings.getProxyServer()).ifPresent((ps) -> {
            Client client = ClientProxy.getClient(result);
            HTTPConduit conduit = (HTTPConduit) client.getConduit();
            HTTPClientPolicy policy = conduit.getClient();

            policy.setProxyServer(ps);
            Optional.ofNullable(settings.getProxyPort()).ifPresent(policy::setProxyServerPort);
            ProxyAuthorizationPolicy pa = conduit.getProxyAuthorization();
            Optional.ofNullable(settings.getProxyUsername()).ifPresent(pa::setUserName);
            Optional.ofNullable(settings.getProxyPassword()).ifPresent(pa::setPassword);
        });

        return result;
    }
}
