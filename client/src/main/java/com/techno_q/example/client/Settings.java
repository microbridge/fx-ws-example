package com.techno_q.example.client;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.OptionalInt;

/**
 * 設定を表すクラスです。
 */
@Component
@ConfigurationProperties(prefix = "settings")
@Data
public class Settings {
    /**
     * 接続する Web サービスの Prefix
     *
     * ex)
     *   http://yourhost.yourdomain/context
     */
    private String webServicePrefix = "http://localhost:8090";

    /**
     * Proxy サーバのアドレス
     */
    private String proxyServer = "";

    /**
     * Proxy サーバのポート番号
     */
    private Integer proxyPort = null;

    /**
     * Proxy 認証用のユーザ名
     */
    private String proxyUsername = "";

    /**
     * Proxy 認証用のパスワード
     */
    private String proxyPassword = "";
}
