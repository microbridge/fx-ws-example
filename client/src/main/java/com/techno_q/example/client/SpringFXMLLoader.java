package com.techno_q.example.client;

import com.techno_q.example.client.controller.AbstractController;
import com.techno_q.example.client.helper.I18nHelper;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.NonNull;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Optional;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Spring を絡めた FXML ローダです。
 */
@Component
public class SpringFXMLLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringFXMLLoader.class);

    /**
     * メッセージリソースバンドルに native2ascii を適用しなくても良くするためのリソースバンドル・コントロール
     */
    private static final ResourceBundle.Control UTF8_ENCODING_CONTROL = new ResourceBundle.Control() {
        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                throws IllegalAccessException, InstantiationException, IOException {
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");

            try (InputStream is = loader.getResourceAsStream(resourceName);
                 InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                 BufferedReader reader = new BufferedReader(isr)) {
                return new PropertyResourceBundle(reader);
            }
        }
    };

    @Autowired
    private ApplicationContext context;

    @Autowired
    private I18nHelper i18nHelper;


    /**
     * FXML をロードします。
     *
     * @param path  FXML のリソースとしてのパス
     * @param stage ロードした FXML をシーンとして設定するステージ
     * @return コントローラオブジェクト
     */
    public <T extends AbstractController> Optional<T> loadFromResource(String path, @NonNull Stage stage) {
        val loader = new FXMLLoader();
        val bundle = ResourceBundle.getBundle("messages", i18nHelper.getLocale(), UTF8_ENCODING_CONTROL);
        loader.setControllerFactory(context::getBean);
        loader.setResources(bundle);

        return Optional.ofNullable(SpringFXMLLoader.class.getClassLoader().getResourceAsStream(path)).map((res) -> {
            Parent parent = null;
            try {
                parent = loader.load(res);
            } catch (IOException e) {
                String message = i18nHelper.getMessage("error.resource.load", path);
                LOGGER.error(message, e);
                throw new RuntimeException(message, e);
            }
            stage.setScene(new Scene(parent));

            return Optional.<T>ofNullable(loader.getController()).map((ac) -> {
                ac.setStage(stage);
                return ac;
            });
        }).orElseThrow(() -> new RuntimeException(i18nHelper.getMessage("error.resource.path", path)));
    }
}
