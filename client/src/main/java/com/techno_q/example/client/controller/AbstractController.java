package com.techno_q.example.client.controller;

import com.techno_q.example.client.SpringFXMLLoader;
import com.techno_q.example.client.helper.I18nHelper;
import javafx.event.EventHandler;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML と紐付けるコントローラの基底クラスです。
 */
public abstract class AbstractController {
    @Setter
    @Getter
    private Stage stage;

    @Getter
    @Autowired
    private SpringFXMLLoader loader;

    @Autowired
    private I18nHelper i18nHelper;

    private EventHandler<WindowEvent> childOnHiddenHandler = (event) -> {
        getStage().show();
    };

    /**
     * 新たなウィンドウを開きます。
     *
     * このコントローラと紐づくウィンドウは非表示になります。
     * モーダリティはモーダルになります。
     * ウィンドウはリサイズ不可となります。
     * クローズ時に親ウィンドウを表示するイベントハンドラが設定されます。
     *
     * これらの設定は戻り値の {@link Stage} オブジェクトによって再設定できます。
     *
     * @param fxmlPath ウィンドウにはめ込む FXML
     * @return 新たに作成したウィンドウ
     */
    Stage createStage(String fxmlPath) {
        getStage().hide();

        val newStage = new Stage();
        newStage.initOwner(getStage());

        loader.loadFromResource(fxmlPath, newStage);
        newStage.setResizable(false);
        newStage.initModality(Modality.WINDOW_MODAL);
        newStage.centerOnScreen();
        newStage.setOnHidden(childOnHiddenHandler);
        newStage.show();

        return newStage;
    }

    /**
     * メッセージリソースからメッセージを取得します。
     * @param code メッセージコード
     * @param args メッセージに埋め込む任意の値
     * @return 取得したメッセージ
     */
    String getMessage(String code, Object...args) {
        return i18nHelper.getMessage(code, args);
    }
}
