package com.techno_q.example.client.controller;

import com.techno_q.example.dto.Account;
import com.techno_q.example.service.AccountService;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.WebServiceException;

/**
 * ログイン画面のコントローラです。
 */
@Component
public class LoginController extends AbstractController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @Autowired
    private AccountService accountService;

    /**
     * 「ログイン」ボタン押下時の挙動です。
     */
    @FXML
    public void onActionLogin() {
        Account a = null;
        try {
            a = accountService.authenticate(username.getText(), password.getText());
        } catch (WebServiceException e) {
            val title = getMessage("authentication.service.error.title");
            LOGGER.error(title, e);
            val alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(getMessage("error"));
            alert.getDialogPane().setHeaderText(title);
            alert.getDialogPane().setContentText(getMessage("authentication.service.error.message"));
            alert.show();

            return;
        }

        if (a == null) {
            val title = getMessage("authentication.failure.title");
            val alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(getMessage("information"));
            alert.getDialogPane().setHeaderText(title);
            alert.getDialogPane().setContentText(getMessage("authentication.failure.message"));
            alert.show();

            return;
        }

        getStage().close();
        ((Stage) getStage().getOwner()).show();

    }
}
