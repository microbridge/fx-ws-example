package com.techno_q.example.client.controller;

import javafx.fxml.FXML;
import org.springframework.stereotype.Component;

/**
 * メインメニュー画面のコントローラです。
 */
@Component
public class MainMenuController extends AbstractController {

    /**
     * 「ユーザ一覧」メニューボタン押下時の挙動です。
     */
    @FXML
    public void onActionUsers() {
        createStage("users.fxml");
    }

    /**
     * 「終了」ボタン押下時の挙動です。
     */
    @FXML
    public void onActionExit() {
        getStage().close();
    }

    /**
     * 「ログアウト」ボタン押下時の挙動です。
     */
    @FXML
    public void onActionLogout() {
        showLogin();
    }

    public void showLogin() {
        createStage("login.fxml").setOnHidden((event) -> {
            // 何もしない（親Windowがhiddenならアプリケーション終了）
        });
    }
}
