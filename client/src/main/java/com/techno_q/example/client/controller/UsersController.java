package com.techno_q.example.client.controller;

import com.techno_q.example.entity.User;
import com.techno_q.example.service.UsersService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * ユーザ一覧画面のコントローラです。
 */
@Component
public class UsersController extends AbstractController implements Initializable {
    @FXML
    private TableView<User> usersTable;

    @FXML
    private TableColumn<User, Integer> idColumn;

    @FXML
    private TableColumn<User, String> nameColumn;

    @FXML
    private TableColumn<User, String> addressColumn;

    @FXML
    private TableColumn<User, LocalDate> birthdayColumn;

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory((cdf) -> new ReadOnlyObjectWrapper<>(cdf.getValue().getId()));
        nameColumn.setCellValueFactory((cdf) -> new ReadOnlyObjectWrapper<>(cdf.getValue().getName()));
        addressColumn.setCellValueFactory((cdf) -> new ReadOnlyObjectWrapper<>(cdf.getValue().getAddress()));
        birthdayColumn.setCellValueFactory((cdf) -> new ReadOnlyObjectWrapper<>(cdf.getValue().getBirthday()));
    }

    @FXML
    public void onClickReadButton() {
        usersTable.getItems().addAll(usersService.select());
    }
}
