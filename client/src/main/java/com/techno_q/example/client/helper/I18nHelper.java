package com.techno_q.example.client.helper;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * i18n に関するヘルパです。
 */
@Component
public class I18nHelper {
    @Setter
    @Getter
    private Locale locale = Locale.getDefault();

    @Autowired
    private MessageSource messageSource;

    /**
     * メッセージリソースからメッセージを取得します。
     * @param code メッセージコード
     * @param args メッセージに埋め込む任意の値
     * @return 取得したメッセージ
     */
    public String getMessage(String code, Object...args) {
        return messageSource.getMessage(code, args, locale);
    }
}
