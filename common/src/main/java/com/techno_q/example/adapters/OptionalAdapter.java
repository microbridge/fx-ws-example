package com.techno_q.example.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Optional;

/**
 * Created by asuka on 2016/06/07.
 */
public class OptionalAdapter<T> extends XmlAdapter<T, Optional<T>> {
    @Override
    public Optional<T> unmarshal(T v) throws Exception {
        return Optional.ofNullable(v);
    }

    @Override
    public T marshal(Optional<T> v) throws Exception {
        return v.orElse(null);
    }
}
