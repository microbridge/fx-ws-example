package com.techno_q.example.dto;

import lombok.Data;

import java.util.List;

/**
 * アカウントを表す DTO です。
 */
@Data
public class Account {
    /** ユーザ名 */
    private String username;
    /** 姓 */
    private String familyName;
    /** 名 */
    private String givenName;
    /** 所属グループ */
    private List<String> groups;
    /** 表示名 */
    private String displayName;
    /** 社員コード */
    private String employeeCode;
}
