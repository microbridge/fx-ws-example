package com.techno_q.example.entity;

import lombok.Data;
import org.seasar.doma.*;


import java.time.LocalDate;

/**
 * Userエンティティクラスです。
 */
@Data
@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String address;
    private LocalDate birthday;
}
