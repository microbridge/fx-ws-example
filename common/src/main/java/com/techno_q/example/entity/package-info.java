/**
 * Userエンティティクラスのパッケージです。
 */
@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class, type = LocalDate.class)
})
package com.techno_q.example.entity;

import com.migesok.jaxb.adapter.javatime.LocalDateXmlAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;