package com.techno_q.example.service;

import com.techno_q.example.dto.Account;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.rmi.Remote;

/**
 * アカウントに関する Web サービスのインタフェースです。
 */
@WebService
public interface AccountService extends Remote {
    /**
     * 認証を行います。
     * @param username ユーザ名
     * @param password パスワード
     * @return 認証に成功した場合アカウント情報。失敗した場合 {@code null}
     */
    Account authenticate(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password);
}
