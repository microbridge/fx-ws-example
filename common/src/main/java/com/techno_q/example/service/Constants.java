package com.techno_q.example.service;

import lombok.val;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * クライアントとサーバで共有する定数値を提供するクラスです。
 */
public final class Constants {
    public static final String CXF_SERVLET_PATH = "/services";
    public static final Map<Class<?>, String> SERVICE_PATH_MAP;

    static {
        val map = new HashMap<Class<?>, String>();

        map.put(AccountService.class, "/account");
        map.put(GreetingService.class, "/greeting");
        map.put(UsersService.class, "/users");

        SERVICE_PATH_MAP = Collections.unmodifiableMap(map);
    }

    private Constants() {
        // nop
    }
}
