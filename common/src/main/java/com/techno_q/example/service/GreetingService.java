package com.techno_q.example.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * サンプル用の挨拶サービスです。
 */
@WebService
public interface GreetingService {
    /**
     * 挨拶します。
     * @param name 挨拶する相手の名前
     * @return 挨拶の文言
     */
    @WebMethod
    String greet(
            @WebParam(name = "name") String name);
}
