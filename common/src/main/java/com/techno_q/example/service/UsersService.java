package com.techno_q.example.service;

import com.techno_q.example.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * usersテーブルに関するサービスです。
 */
@WebService
public interface UsersService {
    /**
     * usersテーブルからデータを取得します。
     * @return データ
     */
    @WebMethod
    List<User> select();

    /**
     * usersテーブルにデータを挿入します。
     * @param user Userエンティティ
     * @return 挿入件数
     */
    @WebMethod
    int insert(User user);

    /**
     * usersテーブルのデータを更新します。
     * @param user Userエンティティ
     * @return 更新件数
     */
    @WebMethod
    int update(User user);

    /**
     * usersテーブルのデータを削除します。
     * @param user Userエンティティ
     * @return 削除件数
     */
    @WebMethod
    int delete(User user);

    /**
     * usersテーブルにデータを追加した後、再度データを追加します。
     * @param user Userエンティティ
     * @return 追加件数
     */
    @WebMethod
    int test(User user);
}
