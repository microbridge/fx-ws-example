package com.techno_q.example.server;

import com.techno_q.example.server.controller.AccountController;
import com.techno_q.example.server.controller.GreetingController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

/**
 * Created by asuka on 2016/06/08.
 */
@Configuration
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(GreetingController.class);
        register(AccountController.class);
    }
}
