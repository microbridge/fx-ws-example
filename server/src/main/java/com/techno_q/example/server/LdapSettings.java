package com.techno_q.example.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 認証に使用する LDAP に関する設定です。
 */
@Data
@Component
@ConfigurationProperties(prefix = "ldap")
public class LdapSettings {
    /**
     * LDAP サーバに関する設定です。
     */
    @Data
    public static class LdapServerSetting {
        /**
         * 認証情報です。
         */
        @Data
        public static class Credential {
            /** 認証に使用する principalName */
            private String principalName;
            /** 認証に使用するパスワード */
            private String password;
        }

        /** LDAP サーバの URL */
        private String url;
        /** ユーザ情報を検索する base DN のリスト */
        private List<String> baseDn;
        /** すべてのユーザやグループの情報の閲覧権限のあるアカウントの認証情報 */
        private Credential manager;
    }

    /**
     * ドメイン未指定時に使用するデフォルトのドメイン
     */
    private String domain;

    /**
     * ドメインごとのサーバ設定
     *
     * キーはドメインのピリオドをアンダースコアにしたものを使用してください。
     */
    private Map<String, LdapServerSetting> servers;
}
