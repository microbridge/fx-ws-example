package com.techno_q.example.server;

import com.techno_q.example.service.GreetingService;
import com.techno_q.example.service.UsersService;
import com.techno_q.example.service.AccountService;
import lombok.val;
import org.apache.cxf.Bus;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import javax.xml.ws.Endpoint;
import java.util.Optional;

import static com.techno_q.example.service.Constants.CXF_SERVLET_PATH;
import static com.techno_q.example.service.Constants.SERVICE_PATH_MAP;

/**
 * サーバ・アプリケーションのエントリポイントです。
 */
@SpringBootApplication(scanBasePackages = {"com.techno_q.example"})
@ImportResource({"classpath:META-INF/cxf/cxf.xml"})
public class ServerApplication extends SpringBootServletInitializer {

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(String[] args) {
        // jul によるロギングを SLF4J に載せるための設定
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        SpringApplication.run(ServerApplication.class, args);
    }

    @Bean
    public ServletRegistrationBean provideServletRegistrationBean() {
        val servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(), CXF_SERVLET_PATH + "/*");
        servletRegistrationBean.setLoadOnStartup(1);
        return servletRegistrationBean;
    }

    @Bean
    public Endpoint provideAccountWSEndpoint() {
        return makeEndpoint(AccountService.class);
    }

    @Bean
    public Endpoint provideGreetingServiceEndpoint() {
        return makeEndpoint(GreetingService.class);
    }

    @Bean
    public Endpoint provideUsersServiceEndpoint() {
        return makeEndpoint(UsersService.class);
    }

    @Bean
    public EmbeddedServletContainerFactory provideEmbeddedServletContainerFactory(Settings settings) {
        return new TomcatEmbeddedServletContainerFactory(settings.getServletContext(),
                settings.getServletContainerPort());
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ServerApplication.class);
    }

    private <T> Endpoint makeEndpoint(Class<T> clazz) {
        val o = Optional.ofNullable(SERVICE_PATH_MAP.get(clazz));

        return o.map((path) -> {
            Bus bus = (Bus) applicationContext.getBean(Bus.DEFAULT_BUS_ID);
            T service = applicationContext.getBean(clazz);
            EndpointImpl endpoint = new EndpointImpl(bus, service);
            endpoint.publish(path);
            endpoint.getServer().getEndpoint().getInInterceptors().add(new LoggingInInterceptor());
            endpoint.getServer().getEndpoint().getOutInterceptors().add(new LoggingOutInterceptor());
            return endpoint;
        }).orElseThrow(() -> new IllegalStateException("紐づくパスが定義されていません"));
    }
}
