package com.techno_q.example.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 設定を表すクラスです。
 */
@Component
@ConfigurationProperties(prefix = "settings")
@Data
public class Settings {
    /**
     * サーブレットコンテキスト
     */
    private String servletContext = "";

    /**
     * 埋め込みサーブレットコンテナを稼働させる際のポート番号
     */
    private int servletContainerPort = 8090;
}
