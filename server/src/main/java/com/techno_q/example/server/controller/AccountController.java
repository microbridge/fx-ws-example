package com.techno_q.example.server.controller;

import com.techno_q.example.dto.Account;
import com.techno_q.example.server.logic.AccountLogic;
import com.techno_q.example.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Account に関するサービスのコントローラです。
 */
@Component
@WebService(
        endpointInterface = "com.techno_q.example.service.AccountService",
        serviceName = "AccountService",
        portName = "AccountPort",
        targetNamespace = "http://service.example.techno_q.com/")
@Path("/account")
public class AccountController implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountLogic accountService;

    @Override
    @WebMethod
    @POST
    @Path("authenticate")
    @Produces({MediaType.APPLICATION_JSON})
    public Account authenticate(
            @FormParam("username") String username,
            @FormParam("password") String password) {
        LOGGER.debug("username = {}", username);
        LOGGER.debug("password = {}", password);

        return accountService.authenticate(username, password).orElse(null);
    }
}
