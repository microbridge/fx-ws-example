package com.techno_q.example.server.controller;

import com.techno_q.example.service.GreetingService;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * 挨拶をするサービスのコントローラです。
 */
@Component
@WebService(
        endpointInterface = "com.techno_q.example.service.GreetingService",
        serviceName = "GreetingService",
        portName = "GreetingPort",
        targetNamespace = "http://service.example.techno_q.com/")
@Path("/greeting")
public class GreetingController implements GreetingService {
    @Override
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{name}")
    public String greet(@PathParam("name") String name) {
        return "Hello, " + name + "!";
    }
}
