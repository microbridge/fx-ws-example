package com.techno_q.example.server.dao;

import com.techno_q.example.entity.User;
import org.seasar.doma.*;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;

/**
 * UserクラスのDAOインタフェースです。
 */
@ConfigAutowireable
@Dao
public interface UserDao {
    @Select
    List<User> selectAll();

    @Insert
    int insert(User user);

    @Update
    int update(User user);

    @Delete
    int delete(User user);
}
