package com.techno_q.example.server.helper;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;

/**
 * LDAP に関するヘルパです。
 */
public interface LdapHelper {
    /**
     * DirContext を取得します。
     *
     * テストの必要に応じてスタブを返すこともできます。
     *
     * @param url LDAP サーバの URL
     * @param principalName 認証で使用する principalName
     * @param password 認証で使用するパスワード
     * @return 取得した DirContext
     * @throws NamingException LDAP 接続の際にエラーが発生した場合。特に認証が失敗した場合はこのサブクラスである
     * {@link javax.naming.AuthenticationException} をスローする
     */
    DirContext getDirContext(String url, String principalName, String password) throws NamingException;
}
