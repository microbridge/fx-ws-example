package com.techno_q.example.server.helper;

import lombok.val;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

/**
 * {@link LdapHelper} のデフォルトの実装です。
 */
@Component
public class LdapHelperImpl implements LdapHelper {
    @Override
    public DirContext getDirContext(String url, String principalName, String password) throws NamingException {
        val env = new Hashtable<Object, Object>();

        // Contextファクトリ
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        // 接続先URL
        env.put(Context.PROVIDER_URL, url);

        // セキュリティレベル
        env.put(Context.SECURITY_AUTHENTICATION, "simple");

        // ユーザID＋ドメイン
        env.put(Context.SECURITY_PRINCIPAL, principalName);

        // パスワード
        env.put(Context.SECURITY_CREDENTIALS, password);

        return new InitialDirContext(env);
    }
}
