package com.techno_q.example.server.logic;

import com.techno_q.example.dto.Account;
import com.techno_q.example.server.LdapSettings;
import com.techno_q.example.server.helper.LdapHelper;
import lombok.NonNull;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * アカウントに関するサービスです。
 */
@Service
public class AccountLogic {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountLogic.class);

    @Autowired
    private LdapSettings ldapSettings;

    @Autowired
    private LdapHelper ldapHelper;

    public Optional<Account> authenticate(@NonNull String username, @NonNull String password) {
        if (username.length() == 0 || password.length() == 0) {
            // ユーザ名またはパスワードがない場合は認証処理をするまでもなく認証失敗
            return Optional.empty();
        }

        LOGGER.debug(ldapSettings.toString());

        // ユーザ名にドメイン名を付加された場合はデフォルトドメイン名を付加して正規化する
        val pn = username.contains("@") ? username : username + "@" + ldapSettings.getDomain();
        // ユーザのドメイン名を取得する
        val domain = getDomainName(username);
        // ドメイン名のピリオドをアンダースコアにして設定を取得
        val serverOpt = Optional.ofNullable(ldapSettings.getServers().get(domain.replace('.', '_')));

        return serverOpt.flatMap((server) -> {
            String url = server.getUrl();
            LOGGER.debug("url = {}", url);
            Optional<DirContext> ctxOpt = getDirContext(url, pn, password);

            return ctxOpt.flatMap((ctx) -> {
                try {
                    LOGGER.debug("認証成功");

                    for (String dn : server.getBaseDn()) {
                        Optional<Account> aOpt = null;
                        try {
                            aOpt = findAccountByPrincipalName(ctx, dn, pn);
                        } catch (NamingException e) {
                            LOGGER.error("各属性の取得に失敗", e);
                            continue;
                        }

                        assert aOpt != null;
                        if (aOpt.isPresent()) {
                            return aOpt;
                        }
                    }

                    return Optional.empty();
                } finally {
                    try {
                        ctx.close();
                    } catch (NamingException e) {
                        LOGGER.error("DirContext のクローズに失敗", e);
                    }
                }
            });

        });
    }

    /**
     * LDAP の接続コンテキストを取得する。
     *
     * @param url           LDAP サーバアドレス
     * @param principalName 認証を行うためのユーザ名
     * @param password      認証を行うためのパスワード
     * @return 取得したコンテキスト
     */
    private Optional<DirContext> getDirContext(String url, String principalName, String password) {
        try {
            return Optional.of(ldapHelper.getDirContext(url, principalName, password));
        } catch (AuthenticationException e) {
            LOGGER.debug("認証失敗", e);
            return Optional.empty();
        } catch (NamingException e) {
            LOGGER.error("認証失敗", e);
            return Optional.empty();
        }
    }

    private String getDomainName(String username) {
        val ss = username.split("@");
        return ss.length == 2 ? ss[1] : ldapSettings.getDomain();
    }

    /**
     * アカウントを取得します。userPrincipalName でフィルタして取得します。検索範囲はサブツリースコープです。
     *
     * @param context  接続コンテキスト
     * @param baseDn   検索時のベース DN
     * @param username ユーザ名
     * @return 取得結果
     */
    private Optional<Account> findAccountByPrincipalName(DirContext context, String baseDn, String username) throws NamingException {
        val filter = "userPrincipalName=" + username;

        val list = findAccount(context, baseDn, filter);

        if (list.isEmpty()) {
            return Optional.empty();
        }

        return Optional.ofNullable(list.get(0));
    }

    private List<Account> findAccount(DirContext context, String baseDn, String filter) throws NamingException {
        return find(context, baseDn, filter, this::getAccount);
    }

    private <A> List<A> find(DirContext context, String baseDn, String filter, Function<SearchResult, Optional<A>> f) throws NamingException {
        val controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        LOGGER.debug("filter = {}", filter);
        LOGGER.debug("baseDn = {}", baseDn);
        val answer = context.search(baseDn, filter, controls);

        val list = new ArrayList<A>();
        while (answer.hasMore()) {
            val result = answer.next();

            f.apply(result).ifPresent(list::add);
        }

        return list;
    }

    private Optional<Account> getAccount(SearchResult result) {
        val attr = result.getAttributes();

        val usernameOpt = getAttributeValue(attr, "userPrincipalName");

        return usernameOpt.map((username) -> {
            List<String> memberOf = Optional.ofNullable(attr.get("memberOf")).map(Stream::of).map((s) -> s.flatMap((a) -> {
                try {
                    return Collections.list(a.getAll()).stream().map(Object::toString);
                } catch (NamingException e) {
                    LOGGER.error("memberOf 取得に失敗", e);
                    return Stream.empty();
                }
            })).map((s) -> s.collect(Collectors.toList())).orElse(new ArrayList<>());
            Optional<String> employeeNumber = getAttributeValue(attr, "employeeNumber");
            Optional<String> sn = getAttributeValue(attr, "sn");
            Optional<String> givenName = getAttributeValue(attr, "givenName");
            Optional<String> displayName = getAttributeValue(attr, "displayName");
            Account a = new Account();
            a.setUsername(username);
            a.setFamilyName(sn.orElse(null));
            a.setGivenName(givenName.orElse(null));
            a.setEmployeeCode(employeeNumber.orElse(null));
            a.setDisplayName(displayName.orElse(null));
            a.setGroups(memberOf);

            LOGGER.debug("result = {}", a);
            return a;
        });
    }

    private Optional<String> getAttributeValue(Attributes attr, String attrID) {
        return Optional.ofNullable(attr.get(attrID)).flatMap((a) -> {
            try {
                return Optional.ofNullable(a.get());
            } catch (NamingException e) {
                LOGGER.error(attrID + " 取得に失敗", e);
                return Optional.empty();
            }
        }).map(Object::toString);
    }
}
