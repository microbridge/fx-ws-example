package com.techno_q.example.service;

import com.techno_q.example.entity.User;
import com.techno_q.example.server.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * usersテーブルに関するサービスの実装です。
 */
@Component
@WebService(endpointInterface = "com.techno_q.example.service.UsersService", serviceName="UsersService")
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserDao dao;

    @Autowired
    UsersTransactionTest test;

    @Override
    @WebMethod
    public List<User> select(){
        return dao.selectAll();
    }

    @Override
    @WebMethod
    public int insert(User user){
        return dao.insert(user);
    }

    @Override
    @WebMethod
    public int update(User user){
        return dao.update(user);
    }

    @Override
    @WebMethod
    public int delete(User user){
        return dao.delete(user);
    }

    @Override
    @WebMethod
    public int test(User user){
        return test.test(user);
    }
}
