package com.techno_q.example.service;

import com.techno_q.example.server.dao.UserDao;
import com.techno_q.example.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * トランザクションのテスト用です。
 */
@Component
public class UsersTransactionTest {
    @Autowired
    UserDao dao;

    @Transactional
    public int test(User user){
        dao.insert(user);
        return dao.insert(user);
    }
}
