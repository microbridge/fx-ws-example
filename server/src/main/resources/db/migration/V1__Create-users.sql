CREATE TABLE users (
  id   IDENTITY,
  name VARCHAR(50),
  address VARCHAR(50),
  birthday DATE
);

INSERT INTO users(id,name,address,birthday) VALUES (1,'Yamada','Yamagata','2000-08-13');