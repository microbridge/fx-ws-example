package com.techno_q.example.service

import spock.lang.Specification

/**
 * GreetingServiceImplのテストクラスです。
 */
class GreetingServiceImplTest extends Specification {
    def"test"(){
        setup:
            def greeting = new GreetingServiceImpl()
            def name = "Taro"

        when:
            def result = greeting.greet(name)

        then:
            result == "Hello, Taro!"
    }

}
