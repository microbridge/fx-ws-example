package com.techno_q.example.service

import com.techno_q.example.server.ServerApplication
import com.techno_q.example.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import spock.lang.Specification

import java.time.LocalDate

/**
 * UsersServiceImplのテストコードです。
 */
@SpringApplicationConfiguration(classes = ServerApplication)
class UsersServiceImplTest extends Specification {
    @Autowired
    UsersServiceImpl impl

    def"test"(){

        when:
        def user = new User()
        user.setName("Hanako")
        user.setAddress("Miyagi")
        def date = LocalDate.of(1999,9,9)
        //def date = new Date(1999-9-9)
        user.setBirthday(date)
        def insert = impl.insert(user)
        user.setAddress("Tokyo")
        def update = impl.update(user)
        def delete = impl.delete(user)

        def select = impl.select()

        then:
        !select.empty
        insert == 1
        update == 1
        delete == 1

    }
}
